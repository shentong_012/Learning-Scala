import org.scalatest.FunSuite

/**
  * @author bill
  *         2017-12-21 16:42
  **/
class CubeCalculatorTest extends FunSuite {
  test("CubeCalculator.cube") {
    assert(CubeCalculator.cube(3) === 27)
  }
  test("CubeCalculator.cube2") {
    assert(CubeCalculator.cube(2) === 8)
  }
}
